package org.antonioxocoy.demo.controllers;

import org.antonioxocoy.demo.entities.User;
import org.antonioxocoy.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/users")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public List<User> getAllUsers() {
        return this.userService.getAllUsers();
    }
    @GetMapping("/{id}")
    public User getAllUsers(@PathVariable("id") Long id, @RequestParam("status") String status) {
        return this.userService.getUser(id);
    }


}
