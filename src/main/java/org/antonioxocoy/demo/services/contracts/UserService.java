package org.antonioxocoy.demo.services.contracts;

import org.antonioxocoy.demo.entities.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
    User getUser(Long id);
    void saveUser(User user);
}
