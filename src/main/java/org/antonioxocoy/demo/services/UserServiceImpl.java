package org.antonioxocoy.demo.services;

import org.antonioxocoy.demo.entities.User;
import org.antonioxocoy.demo.repositories.UserRepository;
import org.antonioxocoy.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow();
    }

    @Override
    public void saveUser(User user) {
        this.userRepository.save(user);
    }
}
